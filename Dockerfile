FROM python

# Port to expose
EXPOSE 7070

# Keeps Python from generating .pyc files in the container
ENV PYTHONDONTWRITEBYTECODE 1

# Turns off buffering for easier container logging
ENV PYTHONUNBUFFERED 1

# Defining the working directory
WORKDIR /app
ADD . /app

# Installing the modules
RUN pip install --upgrade pip
RUN pip install -U pipenv
RUN pipenv install --system

# Running the application using gunicorn
CMD ["gunicorn", "--bind", "0.0.0.0:7070", "paradis.wsgi"]