from rest_framework.response import Response
from rest_framework import status
from .resources import DemoResource, AppleResource, TVShowResource
from .serializers import QueryParamsSerializer


def search_data(request, *args, **kwargs):
    return Response(
        data={
            "message": "The search was successful",
            "searched_data": {
                "person": DemoResource.get_resource(request),
                "artists": AppleResource.get_resource(request),
                "tv_shows": TVShowResource.get_resource(request),
            },
        },
        status=status.HTTP_200_OK,
    )
