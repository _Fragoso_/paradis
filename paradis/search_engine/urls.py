from django.urls import path
from .views import SearchEngineList

urlpatterns = [
    path("search_engine/", SearchEngineList.as_view()),
]
