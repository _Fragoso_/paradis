from dataclasses import dataclass

import requests
from django.conf import settings
from zeep import Client, Settings

DEMO_URL = settings.DEMO_URL
APPLE_URL = settings.APPLE_URL
TV_URL = settings.TV_URL


class DemoService:
    """
    SOAP service that returns social data from a 
    simple WSDL link.
    """

    def __init__(self):
        self.url = DEMO_URL
        self.settings = Settings(strict=False, xml_huge_tree=True)
        self.client = Client(self.url, settings=self.settings)

    def find_person(self, person_id: int) -> object or None:
        try:
            return self.client.service.FindPerson(id=person_id)
        except Exception:
            return None


class AppleService:
    """
    RESTful service that returns artists data
    from an API endpoint.
    """

    def __init__(self):
        self.url = APPLE_URL

    def search_artists_by_term(self, term: str) -> list or None:
        try:
            data_obtained = requests.get(self.url, params={"term": term})
            artists_data = data_obtained.json()["results"]

            return artists_data
        except Exception:
            return None


class TVService:
    """
    RESTful service that returns TV shows data
    from an API endpoint.
    """

    def __init__(self):
        self.url = TV_URL

    def search_tv_shows(self, query: str) -> list or None:
        try:
            tv_data = requests.get(self.url, params={"q": query}).json()

            return tv_data
        except Exception:
            return None
