from dataclasses import dataclass
from typing import List

from rest_framework import serializers


class QueryParamsSerializer(serializers.Serializer):
    person_id = serializers.CharField()
    tv_show_name = serializers.CharField()
    artist_name = serializers.CharField()


@dataclass
class PersonSerializer:

    obj: object

    def _get_name(self) -> str:
        return self.obj.Name

    def _get_age(self) -> str:
        return self.obj.Age

    def _get_date_of_birth(self) -> str:
        return str(self.obj.DOB)

    @property
    def data(self) -> dict:
        return {
            "name": self._get_name(),
            "age": self._get_name(),
            "date_of_birth": self._get_date_of_birth(),
        }


@dataclass
class ArtistSerializer:

    artists_data: List

    @property
    def data(self) -> dict or list:
        if len(self.artists_data) == 1:
            return {
                "artist_name": self.artists_data["artistName"],
                "genre": self.artists_data["primaryGenreName"],
                "country": self.artists_data["country"],
            }

        return [
            {
                "artist_name": artist["artistName"],
                "genre": artist["primaryGenreName"],
                "country": artist["country"],
            }
            for artist in self.artists_data
        ]


@dataclass
class TVShowSerializer:

    tv_data: List

    @property
    def data(self) -> dict or list:
        if len(self.tv_data) == 1:
            return self._format_show_data(self.tv_data["show"])

        return [self._format_show_data(tv_element["show"]) for tv_element in self.tv_data]

    def _format_show_data(self, show_data):
        return {
            "show_name": show_data["name"],
            "genres": show_data["genres"],
            "language": show_data["language"],
            "premiered_date": show_data["premiered"],
        }
