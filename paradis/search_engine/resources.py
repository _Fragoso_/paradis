from .serializers import (
    ArtistSerializer,
    PersonSerializer,
    TVShowSerializer,
    QueryParamsSerializer,
)
from .services import DemoService, AppleService, TVService
from utils.handlers import sort_dict_list


class DemoResource:
    @classmethod
    def get_resource(cls, request) -> dict:
        demo_service = DemoService()
        person_obtained = demo_service.find_person(
            person_id=int(request.GET["person_id"])
        )
        person_serializer = PersonSerializer(person_obtained)

        return person_serializer.data


class AppleResource:
    @classmethod
    def get_resource(cls, request) -> dict or list:
        apple_service = AppleService()
        artists_obtained = apple_service.search_artists_by_term(
            term=request.GET["artist_name"]
        )
        artists_serializer = ArtistSerializer(artists_obtained)

        return sort_dict_list(artists_serializer.data, key="artist_name")


class TVShowResource:
    @classmethod
    def get_resource(cls, request) -> dict or list:
        tv_service = TVService()
        tv_shows_obtained = tv_service.search_tv_shows(query=request.GET["tv_show_name"])
        tv_shows_serializer = TVShowSerializer(tv_shows_obtained)

        return sort_dict_list(tv_shows_serializer.data, key="show_name")
