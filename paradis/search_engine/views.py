from rest_framework.views import APIView
from .controllers import search_data
from decorators.validators import query_params_validation


class SearchEngineList(APIView):
    @query_params_validation
    def get(self, request, *args, **kwargs):
        return search_data(request)
