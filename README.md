# Paradis
**Paradis** is a Django application that consumes the `SOAP` and `REST` web services of various web sources, using the `requests` and `zeep` python libraries.

## Dependencies
![Python +3.7](https://img.shields.io/badge/python-+3.7-blue.svg)
![Docker](https://img.shields.io/badge/docker-19.03.8-blue.svg)
![PostgreSQL](https://img.shields.io/badge/postgres-*-blue.svg)

## Configuration
First, you have to clone this repository with the following command.
```
git clone git@bitbucket.org:_Fragoso_/paradis.git
```

Then, inside the project directory, execute the following command.
```
cp .env.example .env
```
This command create the `.env` file, that will hold the environment variables containing the private access credentials of the **web services**.
The variables contained inside the `.env` file are listed below.

### **Environment variables**

**DEMO_URL**=https://www.crcind.com/csp/samples/SOAP.Demo.cls?WSDL

**APPLE_URL**=https://itunes.apple.com/search

**TV_URL**=http://api.tvmaze.com/search/shows

You need to copy the previous variables into the `.env` file created.


Install the dependencies with the command below.

```
pipenv install --three --dev
```

Go back to the root project directory, and execute the following commands to activate the environment and run the application.

```
pipenv shell --fancy
python3 manage.py runserver
```

Now you have the application running inside your localhost.

If you want to run the Django application inside a Docker container, you want to execute the following commands inside
the project root directory, 

```
docker build -f Dockerfile  -t paradis:1.0 .
docker run --restart=always \
--name=paradis_project --env-file=.env \
-p 8094:7070 -d paradis:1.0 
```

Now you have the following endpoint running inside the Docker container:

**`http://127.0.0.1:8094/search_engine/`**

| URL Param    | Type    | Required |   |   |
|--------------|---------|----------|---|---|
| person_id    | integer | Yes      |   |   |
| tv_show_name | string  | Yes      |   |   |
| artist_name  | string  | Yes      |   |   |


## Example Response

```
http://127.0.0.1:8094/search_engine/?person_id=1&tv_show_name=Jack&artist_name=Harry
``` 

**Response**

```json
{
    "message": "The search was successful",
    "searched_data": {
        "person": {
            "name": "Newton,Dave R.",
            "age": "Newton,Dave R.",
            "date_of_birth": "2000-03-20"
        },
        "artists": [
            {
                "artist_name": "Alfonso Cuarón",
                "genre": "Kids & Family",
                "country": "USA"
            },
            {
                "artist_name": "Anand Tucker",
                "genre": "Romance",
                "country": "USA"
            },
            {
                "artist_name": "Andy Wachowski & Larry Wachowski",
                "genre": "Action & Adventure",
                "country": "USA"
            },
            {
                "artist_name": "Andy Wachowski & Larry Wachowski",
                "genre": "Sci-Fi & Fantasy",
                "country": "USA"
            },
            {
                "artist_name": "Baauer",
                "genre": "Dance",
                "country": "USA"
            },
            {
                "artist_name": "Brad Bird",
                "genre": "Sci-Fi & Fantasy",
                "country": "USA"
            },
            {
                "artist_name": "Bryan Singer",
                "genre": "Action & Adventure",
                "country": "USA"
            },
            {
                "artist_name": "Charles Martin Smith",
                "genre": "Kids & Family",
                "country": "USA"
            },
            {
                "artist_name": "Chris Columbus",
                "genre": "Kids & Family",
                "country": "USA"
            },
            {
                "artist_name": "Chris Columbus",
                "genre": "Kids & Family",
                "country": "USA"
            },
            {
                "artist_name": "Christopher Nolan",
                "genre": "Action & Adventure",
                "country": "USA"
            },
            {
                "artist_name": "David Silverman",
                "genre": "Comedy",
                "country": "USA"
            },
            {
                "artist_name": "David Yates",
                "genre": "Kids & Family",
                "country": "USA"
            },
            {
                "artist_name": "David Yates",
                "genre": "Kids & Family",
                "country": "USA"
            },
            {
                "artist_name": "David Yates",
                "genre": "Kids & Family",
                "country": "USA"
            },
            {
                "artist_name": "David Yates",
                "genre": "Kids & Family",
                "country": "USA"
            },
            {
                "artist_name": "George P. Cosmatos",
                "genre": "Western",
                "country": "USA"
            },
            {
                "artist_name": "Gore Verbinski",
                "genre": "Action & Adventure",
                "country": "USA"
            },
            {
                "artist_name": "Guy Hamilton",
                "genre": "Action & Adventure",
                "country": "USA"
            },
            {
                "artist_name": "Harry Chapin",
                "genre": "Pop",
                "country": "USA"
            },
            {
                "artist_name": "Harry Styles",
                "genre": "Pop",
                "country": "USA"
            },
            {
                "artist_name": "J.K. Rowling",
                "genre": "Fiction",
                "country": "USA"
            },
            {
                "artist_name": "J.K. Rowling",
                "genre": "Fiction",
                "country": "USA"
            },
            {
                "artist_name": "J.K. Rowling",
                "genre": "Fiction",
                "country": "USA"
            },
            {
                "artist_name": "J.K. Rowling",
                "genre": "Fiction",
                "country": "USA"
            },
            {
                "artist_name": "J.K. Rowling",
                "genre": "Fiction",
                "country": "USA"
            },
            {
                "artist_name": "J.K. Rowling",
                "genre": "Fiction",
                "country": "USA"
            },
            {
                "artist_name": "J.K. Rowling",
                "genre": "Fiction",
                "country": "USA"
            },
            {
                "artist_name": "Janis Joplin",
                "genre": "Rock",
                "country": "USA"
            },
            {
                "artist_name": "Kelsy Karter",
                "genre": "Rock",
                "country": "USA"
            },
            {
                "artist_name": "Macy Gray",
                "genre": "R&B/Soul",
                "country": "USA"
            },
            {
                "artist_name": "Malcolm D. Lee",
                "genre": "Comedy",
                "country": "USA"
            },
            {
                "artist_name": "Medison",
                "genre": "Hip-Hop",
                "country": "USA"
            },
            {
                "artist_name": "Michael Connelly",
                "genre": "Mysteries & Thrillers",
                "country": "USA"
            },
            {
                "artist_name": "Michael Connelly",
                "genre": "Mysteries & Thrillers",
                "country": "USA"
            },
            {
                "artist_name": "Michael Connelly",
                "genre": "Mysteries & Thrillers",
                "country": "USA"
            },
            {
                "artist_name": "Michael Connelly",
                "genre": "Mysteries & Thrillers",
                "country": "USA"
            },
            {
                "artist_name": "Michael Connelly",
                "genre": "Mysteries & Thrillers",
                "country": "USA"
            },
            {
                "artist_name": "Mike Newell",
                "genre": "Kids & Family",
                "country": "USA"
            },
            {
                "artist_name": "Peter Hunt",
                "genre": "Action & Adventure",
                "country": "USA"
            },
            {
                "artist_name": "Richard LaGravenese",
                "genre": "Drama",
                "country": "USA"
            },
            {
                "artist_name": "Ridley Scott",
                "genre": "Sci-Fi & Fantasy",
                "country": "USA"
            },
            {
                "artist_name": "Rob Reiner",
                "genre": "Romance",
                "country": "USA"
            },
            {
                "artist_name": "Robert Zemeckis",
                "genre": "Comedy",
                "country": "USA"
            },
            {
                "artist_name": "Sean S. Cunningham",
                "genre": "Horror",
                "country": "USA"
            },
            {
                "artist_name": "Sik-K, pH-1 & Jay Park",
                "genre": "K-Pop",
                "country": "USA"
            },
            {
                "artist_name": "Stephen Gaghan",
                "genre": "Kids & Family",
                "country": "USA"
            },
            {
                "artist_name": "Steven Soderbergh",
                "genre": "Action & Adventure",
                "country": "USA"
            },
            {
                "artist_name": "Terence Young",
                "genre": "Action & Adventure",
                "country": "USA"
            },
            {
                "artist_name": "Tommy Lee Wallace",
                "genre": "Horror",
                "country": "USA"
            }
        ],
        "tv_shows": [
            {
                "show_name": "Gentleman Jack",
                "genres": [
                    "Drama",
                    "Romance",
                    "History"
                ],
                "language": "English",
                "premiered_date": "2019-04-22"
            },
            {
                "show_name": "Jack Dee's Full Mountie",
                "genres": [
                    "Comedy"
                ],
                "language": "English",
                "premiered_date": "2000-02-03"
            },
            {
                "show_name": "Jack Irish",
                "genres": [
                    "Drama",
                    "Crime"
                ],
                "language": "English",
                "premiered_date": "2016-02-11"
            },
            {
                "show_name": "Jack Reacher",
                "genres": [
                    "Drama"
                ],
                "language": "English",
                "premiered_date": null
            },
            {
                "show_name": "Jack Taylor",
                "genres": [
                    "Drama",
                    "Crime",
                    "Mystery"
                ],
                "language": "English",
                "premiered_date": "2010-08-02"
            },
            {
                "show_name": "Outback Jack",
                "genres": [],
                "language": "English",
                "premiered_date": "2004-06-22"
            },
            {
                "show_name": "Samurai Jack",
                "genres": [
                    "Action",
                    "Adventure"
                ],
                "language": "English",
                "premiered_date": "2001-08-10"
            },
            {
                "show_name": "Surviving Jack",
                "genres": [
                    "Comedy",
                    "Family"
                ],
                "language": "English",
                "premiered_date": "2014-03-27"
            },
            {
                "show_name": "Tom Clancy's Jack Ryan",
                "genres": [
                    "Drama",
                    "Action",
                    "Espionage"
                ],
                "language": "English",
                "premiered_date": "2018-08-31"
            },
            {
                "show_name": "Zoe, Duncan, Jack & Jane",
                "genres": [
                    "Comedy"
                ],
                "language": "English",
                "premiered_date": "1999-01-17"
            }
        ]
    }
}
```

## Postman result
The result obtained in `Postman` is shown in the following image.

![](results/search_api_result.png)