def sort_dict_list(dict_list: list, key: str, reverse=False) -> list:
    return sorted(
        dict_list, key=lambda list_element: list_element[key], reverse=reverse
    )

