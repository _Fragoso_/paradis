from paradis.search_engine.serializers import QueryParamsSerializer


def query_params_validation(main_function):
    """
    Function decorator that validates the entered 
    query params after execute the search logic.
    """

    def inner_validation(self, request, *args, **kwargs):
        query_params_serializer = QueryParamsSerializer(data=request.GET)
        query_params_serializer.is_valid(raise_exception=True)

        return main_function(self, request, *args, **kwargs)

    return inner_validation
